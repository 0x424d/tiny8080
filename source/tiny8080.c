#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "instructions/arithmetic.h"
#include "instructions/branch.h"
#include "instructions/logical.h"
#include "instructions/stack_io_control.h"
#include "instructions/transfer.h"

void Interrupt(State* state, int n)
{
	// PUSH PC then set PC to low memory vector to handle the interrupt
	mne_PUSH(state, 'C');
	mne_RST(state, n);
}

void UnknownInstruction(State* state)
{
	printf("Error: Instruction not implemented\n");
	exit(1);
}

int Emulate8080(State* state)
{
	// opcode is a pointer to the current instruction in memory.
	// opcode[1] will be the byte that immediately follows it.
	// opcode[2] will be the byte 2 places in front of opcode.
	// This is because opcode[n] is synatactic sugar for *(opcode+n).
	BYTE* opcode = &(state->memory[state->pc]);

	switch (*opcode)
	{
		case 0x00: state->pc += 1;              break; // NOP
		case 0x01: mne_LXI(state, 'B', opcode); break; // LXI B, D16
		case 0x02: mne_STAX(state, 'B');        break; // STAX B
		case 0x03: mne_INX(state, 'B');         break; // INX B
		case 0x04: mne_INR(state, 'B');         break; // INR B
		case 0x05: mne_DCR(state, 'B');         break; // DCR B
		case 0x06: mne_MVI(state, 'B', opcode); break; // MVI B, D8
		case 0x07: mne_RLC(state);              break; // RLC
		case 0x08: UnknownInstruction(state);   break; // -
		case 0x09: mne_DAD(state, 'B');         break; // DAD B
		case 0x0A: mne_LDAX(state, 'B');        break; // LDAX B
		case 0x0B: mne_DCX(state, 'B');         break; // DCX B
		case 0x0C: mne_INR(state, 'C');         break; // INR C
		case 0x0D: mne_DCR(state, 'C');         break; // DCR C
		case 0x0E: mne_MVI(state, 'C', opcode); break; // MVI C, D8
		case 0x0F: mne_RRC(state);              break; // RRC
		case 0x10: UnknownInstruction(state);   break; // -
		case 0x11: mne_LXI(state, 'D', opcode); break; // LXI D, D16
		case 0x12: mne_STAX(state, 'D');        break; // STAX D
		case 0x13: mne_INX(state, 'D');         break; // INX D
		case 0x14: mne_INR(state, 'D');         break; // INR D
		case 0x15: mne_DCR(state, 'D');         break; // DCR D
		case 0x16: mne_MVI(state, 'D', opcode); break; // MVI D, D8
		case 0x17: mne_RAL(state);              break; // RAL
		case 0x18: UnknownInstruction(state);   break; // -
		case 0x19: mne_DAD(state, 'D');         break; // DAD D
		case 0x1A: mne_LDAX(state, 'D');        break; // LDAX D
		case 0x1B: mne_DCX(state, 'D');         break; // DCX D
		case 0x1C: mne_INR(state, 'E');         break; // INR E
		case 0x1D: mne_DCR(state, 'E');         break; // DCR E
		case 0x1E: mne_MVI(state, 'E', opcode); break; // MVI E, D8
		case 0x1F: mne_RAR(state);              break; // RAR
		case 0x20: UnknownInstruction(state);   break; // -
		case 0x21: mne_LXI(state, 'H', opcode); break; // LXI H, D16
		case 0x22: mne_SHLD(state, opcode);     break; // SHLD A16
		case 0x23: mne_INX(state, 'H');         break; // INX H
		case 0x24: mne_INR(state, 'H');         break; // INR H
		case 0x25: mne_DCR(state, 'H');         break; // DCR H
		case 0x26: mne_MVI(state, 'H', opcode); break; // MVI H, D8
		case 0x27: mne_DAA(state);              break; // DAA
		case 0x28: UnknownInstruction(state);   break; // -
		case 0x29: mne_DAD(state, 'H');         break; // DAD H
		case 0x2A: mne_LHLD(state, opcode);     break; // LHLD A16
		case 0x2B: mne_DCX(state, 'H');         break; // DCX H
		case 0x2C: mne_INR(state, 'L');         break; // INR L
		case 0x2D: mne_DCR(state, 'L');         break; // DCR L
		case 0x2E: mne_MVI(state, 'L', opcode); break; // MVI L, D8
		case 0x2F: mne_CMA(state);              break; // CMA
		case 0x30: UnknownInstruction(state);   break; // -
		case 0x31: mne_LXI(state, 'S', opcode); break; // LXI SP, D16
		case 0x32: mne_STA(state, opcode);      break; // STA A16
		case 0x33: mne_INX(state, 'S');         break; // INX SP
		case 0x34: mne_INR(state, 'M');         break; // INR M
		case 0x35: mne_DCR(state, 'M');         break; // DCR M
		case 0x36: mne_MVI(state, 'M', opcode); break; // MVI M, D8
		case 0x37: mne_STC(state);              break; // STC
		case 0x38: UnknownInstruction(state);   break; // -
		case 0x39: mne_DAD(state, 'S');         break; // DAD SP
		case 0x3A: mne_LDA(state, opcode);      break; // LDA A16
		case 0x3B: mne_DCX(state, 'S');         break; // DCX SP
		case 0x3C: mne_INR(state, 'A');         break; // INR A
		case 0x3D: mne_DCR(state, 'A');         break; // DCR A
		case 0x3E: mne_MVI(state, 'A', opcode); break; // MVI A, D8
		case 0x3F: mne_CMC(state);              break; // CMC
		case 0x40: mne_MOV(state, 'B', 'B');    break; // MOV B, B
		case 0x41: mne_MOV(state, 'B', 'C');    break; // MOV B, C
		case 0x42: mne_MOV(state, 'B', 'D');    break; // MOV B, D
		case 0x43: mne_MOV(state, 'B', 'E');    break; // MOV B, E
		case 0x44: mne_MOV(state, 'B', 'H');    break; // MOV B, H
		case 0x45: mne_MOV(state, 'B', 'L');    break; // MOV B, L
		case 0x46: mne_MOV(state, 'B', 'M');    break; // MOV B, M
		case 0x47: mne_MOV(state, 'B', 'A');    break; // MOV B, A
		case 0x48: mne_MOV(state, 'C', 'B');    break; // MOV C, B
		case 0x49: mne_MOV(state, 'C', 'C');    break; // MOV C, C
		case 0x4A: mne_MOV(state, 'C', 'D');    break; // MOV C, D
		case 0x4B: mne_MOV(state, 'C', 'E');    break; // MOV C, E
		case 0x4C: mne_MOV(state, 'C', 'H');    break; // MOV C, H
		case 0x4D: mne_MOV(state, 'C', 'L');    break; // MOV C, L
		case 0x4E: mne_MOV(state, 'C', 'M');    break; // MOV C, M
		case 0x4F: mne_MOV(state, 'C', 'A');    break; // MOV C, A
		case 0x50: mne_MOV(state, 'D', 'B');    break; // MOV D, B
		case 0x51: mne_MOV(state, 'D', 'C');    break; // MOV D, C
		case 0x52: mne_MOV(state, 'D', 'D');    break; // MOV D, D
		case 0x53: mne_MOV(state, 'D', 'E');    break; // MOV D, E
		case 0x54: mne_MOV(state, 'D', 'H');    break; // MOV D, H
		case 0x55: mne_MOV(state, 'D', 'L');    break; // MOV D, L
		case 0x56: mne_MOV(state, 'D', 'M');    break; // MOV D, M
		case 0x57: mne_MOV(state, 'D', 'A');    break; // MOV D, A
		case 0x58: mne_MOV(state, 'E', 'B');    break; // MOV E, B
		case 0x59: mne_MOV(state, 'E', 'C');    break; // MOV E, C
		case 0x5A: mne_MOV(state, 'E', 'D');    break; // MOV E, D
		case 0x5B: mne_MOV(state, 'E', 'E');    break; // MOV E, E
		case 0x5C: mne_MOV(state, 'E', 'H');    break; // MOV E, H
		case 0x5D: mne_MOV(state, 'E', 'L');    break; // MOV E, L
		case 0x5E: mne_MOV(state, 'E', 'M');    break; // MOV E, M
		case 0x5F: mne_MOV(state, 'E', 'A');    break; // MOV E, A
		case 0x60: mne_MOV(state, 'H', 'B');    break; // MOV H, B
		case 0x61: mne_MOV(state, 'H', 'C');    break; // MOV H, C
		case 0x62: mne_MOV(state, 'H', 'D');    break; // MOV H, D
		case 0x63: mne_MOV(state, 'H', 'E');    break; // MOV H, E
		case 0x64: mne_MOV(state, 'H', 'H');    break; // MOV H, H
		case 0x65: mne_MOV(state, 'H', 'L');    break; // MOV H, L
		case 0x66: mne_MOV(state, 'H', 'M');    break; // MOV H, M
		case 0x67: mne_MOV(state, 'H', 'A');    break; // MOV H, A
		case 0x68: mne_MOV(state, 'L', 'B');    break; // MOV L, B
		case 0x69: mne_MOV(state, 'L', 'C');    break; // MOV L, C
		case 0x6A: mne_MOV(state, 'L', 'D');    break; // MOV L, D
		case 0x6B: mne_MOV(state, 'L', 'E');    break; // MOV L, E
		case 0x6C: mne_MOV(state, 'L', 'H');    break; // MOV L, H
		case 0x6D: mne_MOV(state, 'L', 'L');    break; // MOV L, L
		case 0x6E: mne_MOV(state, 'L', 'M');    break; // MOV L, M
		case 0x6F: mne_MOV(state, 'L', 'A');    break; // MOV L, A
		case 0x70: mne_MOV(state, 'M', 'B');    break; // MOV M, B
		case 0x71: mne_MOV(state, 'M', 'C');    break; // MOV M, C
		case 0x72: mne_MOV(state, 'M', 'D');    break; // MOV M, D
		case 0x73: mne_MOV(state, 'M', 'E');    break; // MOV M, E
		case 0x74: mne_MOV(state, 'M', 'H');    break; // MOV M, H
		case 0x75: mne_MOV(state, 'M', 'L');    break; // MOV M, L
		case 0x76: mne_HLT(state);              break; // HLT
		case 0x77: mne_MOV(state, 'M', 'A');    break; // MOV M, A
		case 0x78: mne_MOV(state, 'A', 'B');    break; // MOV A, B
		case 0x79: mne_MOV(state, 'A', 'C');    break; // MOV A, C
		case 0x7A: mne_MOV(state, 'A', 'D');    break; // MOV A, D
		case 0x7B: mne_MOV(state, 'A', 'E');    break; // MOV A, E
		case 0x7C: mne_MOV(state, 'A', 'H');    break; // MOV A, H
		case 0x7D: mne_MOV(state, 'A', 'L');    break; // MOV A, L
		case 0x7E: mne_MOV(state, 'A', 'M');    break; // MOV A, M
		case 0x7F: mne_MOV(state, 'A', 'A');    break; // MOV А, А
		case 0x80: mne_ADD(state, 'B');         break; // ADD B
		case 0x81: mne_ADD(state, 'C');         break; // ADD C
		case 0x82: mne_ADD(state, 'D');         break; // ADD D
		case 0x83: mne_ADD(state, 'E');         break; // ADD E
		case 0x84: mne_ADD(state, 'H');         break; // ADD H
		case 0x85: mne_ADD(state, 'L');         break; // ADD L
		case 0x86: mne_ADD(state, 'M');         break; // ADD M
		case 0x87: mne_ADD(state, 'A');         break; // ADD A
		case 0x88: mne_ADC(state, 'B');         break; // ADC B
		case 0x89: mne_ADC(state, 'C');         break; // ADC C
		case 0x8A: mne_ADC(state, 'D');         break; // ADC D
		case 0x8B: mne_ADC(state, 'E');         break; // ADC E
		case 0x8C: mne_ADC(state, 'H');         break; // ADC H
		case 0x8D: mne_ADC(state, 'L');         break; // ADC L
		case 0x8E: mne_ADC(state, 'M');         break; // ADC M
		case 0x8F: mne_ADC(state, 'A');         break; // ADC A
		case 0x90: mne_SUB(state, 'B');         break; // SUB B
		case 0x91: mne_SUB(state, 'C');         break; // SUB C
		case 0x92: mne_SUB(state, 'D');         break; // SUB D
		case 0x93: mne_SUB(state, 'E');         break; // SUB E
		case 0x94: mne_SUB(state, 'H');         break; // SUB H
		case 0x95: mne_SUB(state, 'L');         break; // SUB L
		case 0x96: mne_SUB(state, 'M');         break; // SUB M
		case 0x97: mne_SUB(state, 'A');         break; // SUB A
		case 0x98: mne_SBB(state, 'B');         break; // SBB B
		case 0x99: mne_SBB(state, 'C');         break; // SBB C
		case 0x9A: mne_SBB(state, 'D');         break; // SBB D
		case 0x9B: mne_SBB(state, 'E');         break; // SBB E
		case 0x9C: mne_SBB(state, 'H');         break; // SBB H
		case 0x9D: mne_SBB(state, 'L');         break; // SBB L
		case 0x9E: mne_SBB(state, 'M');         break; // SBB M
		case 0x9F: mne_SBB(state, 'A');         break; // SBB A
		case 0xA0: mne_ANA(state, 'B');         break; // ANA B
		case 0xA1: mne_ANA(state, 'C');         break; // ANA C
		case 0xA2: mne_ANA(state, 'D');         break; // ANA D
		case 0xA3: mne_ANA(state, 'E');         break; // ANA E
		case 0xA4: mne_ANA(state, 'H');         break; // ANA H
		case 0xA5: mne_ANA(state, 'L');         break; // ANA L
		case 0xA6: mne_ANA(state, 'M');         break; // ANA M
		case 0xA7: mne_ANA(state, 'A');         break; // ANA A
		case 0xA8: mne_XRA(state, 'B');         break; // XRA B
		case 0xA9: mne_XRA(state, 'C');         break; // XRA C
		case 0xAA: mne_XRA(state, 'D');         break; // XRA D
		case 0xAB: mne_XRA(state, 'E');         break; // XRA E
		case 0xAC: mne_XRA(state, 'H');         break; // XRA H
		case 0xAD: mne_XRA(state, 'L');         break; // XRA L
		case 0xAE: mne_XRA(state, 'M');         break; // XRA M
		case 0xAF: mne_XRA(state, 'A');         break; // XRA A
		case 0xB0: mne_ORA(state, 'B');         break; // ORA B
		case 0xB1: mne_ORA(state, 'C');         break; // ORA C
		case 0xB2: mne_ORA(state, 'D');         break; // ORA D
		case 0xB3: mne_ORA(state, 'E');         break; // ORA E
		case 0xB4: mne_ORA(state, 'H');         break; // ORA H
		case 0xB5: mne_ORA(state, 'L');         break; // ORA L
		case 0xB6: mne_ORA(state, 'M');         break; // ORA M
		case 0xB7: mne_ORA(state, 'A');         break; // ORA A
		case 0xB8: mne_CMP(state, 'B');         break; // CMP B
		case 0xB9: mne_CMP(state, 'C');         break; // CMP C
		case 0xBA: mne_CMP(state, 'D');         break; // CMP D
		case 0xBB: mne_CMP(state, 'E');         break; // CMP E
		case 0xBC: mne_CMP(state, 'H');         break; // CMP H
		case 0xBD: mne_CMP(state, 'L');         break; // CMP L
		case 0xBE: mne_CMP(state, 'M');         break; // CMP M
		case 0xBF: mne_CMP(state, 'A');         break; // CMP A
		case 0xC0: mne_R(state, 1);             break; // RNZ
		case 0xC1: mne_POP(state, 'B');         break; // POP B
		case 0xC2: mne_J(state, 1, opcode);     break; // JNZ A16
		case 0xC3: mne_J(state, 0, opcode);     break; // JMP A16
		case 0xC4: mne_C(state, 1, opcode);     break; // CNZ A16
		case 0xC5: mne_PUSH(state, 'B');        break; // PUSH B
		case 0xC6: mne_ADI(state, opcode);      break; // ADI D8
		case 0xC7: mne_RST(state, 0);           break; // RST 0
		case 0xC8: mne_R(state, 2);             break; // RZ
		case 0xC9: mne_R(state, 0);             break; // RET
		case 0xCA: mne_J(state, 2, opcode);     break; // JZ A16
		case 0xCB: UnknownInstruction(state);   break; // -
		case 0xCC: mne_C(state, 2, opcode);     break; // CZ A16
		case 0xCD: mne_C(state, 0, opcode);     break; // CALL A16
		case 0xCE: mne_ACI(state, opcode);      break; // ACI D8
		case 0xCF: mne_RST(state, 1);           break; // RST 1
		case 0xD0: mne_R(state, 3);             break; // RNC
		case 0xD1: mne_POP(state, 'D');         break; // POP D
		case 0xD2: mne_J(state, 3, opcode);     break; // JNC A16
		case 0xD3: mne_OUT(state, opcode);      break; // OUT D8
		case 0xD4: mne_C(state, 3, opcode);     break; // CNC A16
		case 0xD5: mne_PUSH(state, 'D');        break; // PUSH D
		case 0xD6: mne_SUI(state, opcode);      break; // SUI D8
		case 0xD7: mne_RST(state, 2);           break; // RST 2
		case 0xD8: mne_R(state, 4);             break; // RC
		case 0xD9: UnknownInstruction(state);   break; // -
		case 0xDA: mne_J(state, 4, opcode);     break; // JC A16
		case 0xDB: mne_IN(state, opcode);       break; // IN D8
		case 0xDC: mne_C(state, 4, opcode);     break; // CC A16
		case 0xDD: UnknownInstruction(state);   break; // -
		case 0xDE: mne_SBI(state, opcode);      break; // SBI D8
		case 0xDF: mne_RST(state, 3);           break; // RST 3
		case 0xE0: mne_R(state, 5);             break; // RPO
		case 0xE1: mne_POP(state, 'H');         break; // POP H
		case 0xE2: mne_J(state, 5, opcode);     break; // JPO A16
		case 0xE3: mne_XTHL(state);             break; // XTHL
		case 0xE4: mne_C(state, 5, opcode);     break; // CPO A16
		case 0xE5: mne_PUSH(state, 'H');        break; // PUSH H
		case 0xE6: mne_ANI(state, opcode);      break; // ANI D8
		case 0xE7: mne_RST(state, 4);           break; // RST 4
		case 0xE8: mne_R(state, 6);             break; // RPE
		case 0xE9: mne_PCHL(state);             break; // PCHL
		case 0xEA: mne_J(state, 6, opcode);     break; // JPE A16
		case 0xEB: mne_XCHG(state);             break; // XCHG
		case 0xEC: mne_C(state, 6, opcode);     break; // CPE A16
		case 0xED: UnknownInstruction(state);   break; // -
		case 0xEE: mne_XRI(state, opcode);      break; // XRI D8
		case 0xEF: mne_RST(state, 5);           break; // RST 5
		case 0xF0: mne_R(state, 7);             break; // RP
		case 0xF1: mne_POP(state, 'P');         break; // POP PSW
		case 0xF2: mne_J(state, 7, opcode);     break; // JP A16
		case 0xF3: mne_DI(state);               break; // DI
		case 0xF4: mne_C(state, 7, opcode);     break; // CP A16
		case 0xF5: mne_PUSH(state, 'P');        break; // PUSH PSW
		case 0xF6: mne_ORI(state, opcode);      break; // ORI D8
		case 0xF7: mne_RST(state, 6);           break; // RST 6
		case 0xF8: mne_R(state, 8);             break; // RM
		case 0xF9: mne_SPHL(state);             break; // SPHL
		case 0xFA: mne_J(state, 8, opcode);     break; // JM A16
		case 0xFB: mne_EI(state);               break; // EI
		case 0xFC: mne_C(state, 8, opcode);     break; // CM A16
		case 0xFD: UnknownInstruction(state);   break; // -
		case 0xFE: mne_CPI(state, opcode);      break; // CPI D8
		case 0xFF: mne_RST(state, 7);           break; // RST 7
	}
	return 0;
}

int main(int argc, char* argv[])
{

	if (argc != 2)
	{
		printf("Usage: ./tiny8080 <rom>\n");
		exit(3);
	}

	State state;

	state.A  = 0;
	state.B  = 0;
	state.C  = 0;
	state.D  = 0;
	state.E  = 0;
	state.H  = 0;
	state.L  = 0;
	state.sp = 0;
	state.pc = 0;
	state.interrupt_enable = 0;

	state.flag.S   = 0;
	state.flag.Z   = 0;
	state.flag.ac  = 0;
	state.flag.P   = 0;
	state.flag.C   = 0;
	state.flag.pad = 0;

	FILE* r = fopen(argv[1], "rb");
	fseek(r, 0, SEEK_END);
	long int size = ftell(r);
	fseek(r, 0, SEEK_SET);
	state.memory = (BYTE*)malloc(size);
	fread(state.memory, sizeof(BYTE), size/sizeof(BYTE), r);

	BYTE i = 0;
	BYTE done = 0;

	clock_t lastInterrupt = 0;

	while (done == 0)
	{
		clock_t c = clock() / CLOCKS_PER_SEC
		if ((float)c - (float)lastInterrupt > 1/60)
		{
			if (state->interrupt_enable)
			{
				Interrupt(state, 2);
				lastInterrupt = c;
			}
		}

		BYTE* opcode = &(state.memory[state.pc]);
		printf("%i. %08X\t%02X %02X %02X\n", i, state.pc, *opcode, opcode[2], opcode[1]);
		i += 1;
		done = Emulate8080(&state);
	}

	free(state.memory);
	return 0;
}

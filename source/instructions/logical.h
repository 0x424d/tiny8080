#include "../util.h"

void mne_ANA(State* state, char reg);
void mne_ANI(State* state, BYTE* opcode);

void mne_XRA(State* state, char reg);
void mne_XRI(State* state, BYTE* opcode);

void mne_ORA(State* state, char reg);
void mne_ORI(State* state, BYTE* opcode);

void mne_CMP(State* state, char reg);
void mne_CPI(State* state, BYTE* opcode);

void mne_RLC(State* state);
void mne_RRC(State* state);
void mne_RAL(State* state);
void mne_RAR(State* state);

void mne_CMA(State* state);
void mne_CMC(State* state);
void mne_STC(State* state);

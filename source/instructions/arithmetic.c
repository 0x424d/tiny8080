#include "arithmetic.h"

// *A = *A + *r
void mne_ADD(State* state, char reg)
{
	// WORD instead of BYTE allows us to capture the carry, if it exists
	WORD answer, combined;

	switch (reg)
	{
		case 'A': answer = (WORD)state->A + (WORD)state->A; break;
		case 'B': answer = (WORD)state->A + (WORD)state->B; break;
		case 'C': answer = (WORD)state->A + (WORD)state->C; break;
		case 'D': answer = (WORD)state->A + (WORD)state->D; break;
		case 'E': answer = (WORD)state->A + (WORD)state->E; break;
		case 'H': answer = (WORD)state->A + (WORD)state->H; break;
		case 'L': answer = (WORD)state->A + (WORD)state->L; break;
		case 'M': // *A = *A + *(*H *L)
			  combined = (state->H << 8) | (state->L);
			  answer = (WORD)state->A + state->memory[combined];
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer > 0xFF);

	// Resets A to a BYTE instead of a WORD
	state->A = answer & 0xFF;

	state->pc += 1;
}

// *A = *A + *(byte 2)
void mne_ADI(State* state, BYTE* opcode)
{
	WORD answer = (WORD)state->A + (WORD)opcode[1];

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer > 0xFF);

	state->A   = answer & 0xFF;

	state->pc += 2;
}

// *A = *A + *r + *carry
void mne_ADC(State* state, char reg)
{
	WORD answer, combined;
	switch (reg)
	{
		case 'A': answer = (WORD)state->A + (WORD)state->A + state->flag.C; break;
		case 'B': answer = (WORD)state->A + (WORD)state->B + state->flag.C; break;
		case 'C': answer = (WORD)state->A + (WORD)state->C + state->flag.C; break;
		case 'D': answer = (WORD)state->A + (WORD)state->D + state->flag.C; break;
		case 'E': answer = (WORD)state->A + (WORD)state->E + state->flag.C; break;
		case 'H': answer = (WORD)state->A + (WORD)state->H + state->flag.C; break;
		case 'L': answer = (WORD)state->A + (WORD)state->L + state->flag.C; break;
		case 'M': combined = (state->H << 8) | (state->L);
			  answer = (WORD)state->A + state->memory[combined] + state->flag.C;
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer > 0xFF);

	state->A = answer & 0xFF;

	state->pc += 1;
}

// *A = *A + *(byte 2) + *carry
void mne_ACI(State* state, BYTE* opcode)
{
	WORD answer = (WORD)state->A + (WORD)opcode[1] + state->flag.C;

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer > 0xFF);

	state->A   = answer & 0xFF;

	state->pc += 2;
}

// *A = *A - *r
void mne_SUB(State* state, char reg)
{
	WORD answer, combined;
	switch (reg)
	{
		case 'A': answer = (WORD)state->A - (WORD)state->A; break;
		case 'B': answer = (WORD)state->A - (WORD)state->B; break;
		case 'C': answer = (WORD)state->A - (WORD)state->C; break;
		case 'D': answer = (WORD)state->A - (WORD)state->D; break;
		case 'E': answer = (WORD)state->A - (WORD)state->E; break;
		case 'H': answer = (WORD)state->A - (WORD)state->H; break;
		case 'L': answer = (WORD)state->A - (WORD)state->L; break;
		case 'M': combined = (state->H << 8) | (state->L);
			  answer = (WORD)state->A - state->memory[combined];
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	// When subtracting, if there is no carry bit C is set
	state->flag.C  = ((answer <= 0xFF));

	state->A = answer & 0xFF;

	state->pc += 1;
}

// *A = *A - *(byte 2)
void mne_SUI(State* state, BYTE* opcode)
{
	WORD answer = (WORD)state->A - (WORD)opcode[1];

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer <= 0xFF);

	state->A   = answer & 0xFF;

	state->pc += 2;
}

// *A = *A - *r - *carry
void mne_SBB(State* state, char reg)
{
	WORD answer, combined;
	switch (reg)
	{
		case 'A': answer = (WORD)state->A - (WORD)state->A - state->flag.C; break;
		case 'B': answer = (WORD)state->A - (WORD)state->B - state->flag.C; break;
		case 'C': answer = (WORD)state->A - (WORD)state->C - state->flag.C; break;
		case 'D': answer = (WORD)state->A - (WORD)state->D - state->flag.C; break;
		case 'E': answer = (WORD)state->A - (WORD)state->E - state->flag.C; break;
		case 'H': answer = (WORD)state->A - (WORD)state->H - state->flag.C; break;
		case 'L': answer = (WORD)state->A - (WORD)state->L - state->flag.C; break;
		case 'M': combined = (state->H << 8) | (state->L);
			  answer = (WORD)state->A - state->memory[combined] - state->flag.C;
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer <= 0xFF);

	state->A = answer & 0xFF;

	state->pc += 1;
}

// *A = *A - *(byte 2) - *carry
void mne_SBI(State* state, BYTE* opcode)
{
	WORD answer = (WORD)state->A - (WORD)opcode[1] - state->flag.C;

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = ((answer & 0xFF) == 0);
	((answer > 0x0F) && (state->A < 0x10)) ? (state->flag.ac = 1) : (state->flag.ac=0);
	state->flag.P  = bParity(answer & 0xFF);
	state->flag.C  = (answer <= 0xFF);

	state->A   = answer & 0xFF;

	state->pc += 2;
}

// *r = *r + #1
void mne_INR(State* state, char reg)
{
	// Doesn't affect the carry bit so we can just use BYTE here
	BYTE answer;
	WORD combined;

	switch (reg)
	{
		case 'A': answer = state->A + 1; state->A += 1; break;
		case 'B': answer = state->B + 1; state->B += 1; break;
		case 'C': answer = state->C + 1; state->C += 1; break;
		case 'D': answer = state->D + 1; state->D += 1; break;
		case 'E': answer = state->E + 1; state->E += 1; break;
		case 'H': answer = state->H + 1; state->H += 1; break;
		case 'L': answer = state->L + 1; state->L += 1; break;
		case 'M': //*(*H *L) = *(*H *L) + 1
			  combined = (state->H << 8) | (state->L);
			  answer = state->memory[combined] + 1;
			  state->memory[combined] += 1;
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = (answer == 0x10);
	state->flag.P  = bParity(answer);

	state->pc += 1;
}

// *r = *r - #1
void mne_DCR(State* state, char reg)
{
	BYTE answer;
	WORD combined;
	switch (reg)
	{
		case 'A': answer = state->A - 1; state->A -= 1; break;
		case 'B': answer = state->B - 1; state->B -= 1; break;
		case 'C': answer = state->C - 1; state->C -= 1; break;
		case 'D': answer = state->D - 1; state->D -= 1; break;
		case 'E': answer = state->E - 1; state->E -= 1; break;
		case 'H': answer = state->H - 1; state->H -= 1; break;
		case 'L': answer = state->L - 1; state->L -= 1; break;
		case 'M': combined = (state->H << 8) | (state->L);
			  answer = state->memory[combined] - 1;
			  state->memory[combined] -= 1;
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = (answer == 0x0F);
	state->flag.P  = bParity(answer);

	state->pc += 1;
}

// INX and DCX operate on two different registers.
// For these mnemonics, "B" means registers B and C, "D" means registers D
// and E, "H" means registers H and L, and "S" means the stack pointer.
// No flags are affected.

// *rh = *rh + #1,
// *rl = *rl + #1
void mne_INX(State* state, char regPair)
{
	switch (regPair)
	{
		case 'B': state->B  += 1; state->C += 1; break;
		case 'D': state->D  += 1; state->E += 1; break;
		case 'H': state->H  += 1; state->L += 1; break;
		case 'S': state->sp += 1;                break;
	}

	state->pc += 1;
}

// *rh = *rh - #1
// *rl = *rl - #1
void mne_DCX(State* state, char regPair)
{
	switch (regPair)
	{
		case 'B': state->B  -= 1; state->C -= 1; break;
		case 'D': state->D  -= 1; state->E -= 1; break;
		case 'H': state->H  -= 1; state->L -= 1; break;
		case 'S': state->sp -= 1;                break;
	}

	state->pc += 1;
}

// *H *L = *H *L + *rh *rl
void mne_DAD(State* state, char regPair)
{
	WORD combine1 = (state->H << 8) | state->L;
	WORD combine2;
	DWORD answer;
	switch (regPair)
	{
		case 'B': combine2 = (state->B << 8) | state->C; break;
		case 'D': combine2 = (state->D << 8) | state->E; break;
		case 'H': combine2 = (state->H << 8) | state->L; break;
		case 'S': combine2 = state->sp;
			  break;
	}

	answer = combine1 + combine2;

	// Only the carry bit is affected
	state->flag.C = (answer > 0xFFFF);

	state->H = answer & 0xFF00;
	state->L = answer & 0x00FF;

	state->pc += 1;
}


void mne_DAA(State* state)
{
	WORD answer;
	if (((state->A & 0x00FF) > 9) || (state->flag.ac))
	{
		answer = state->A + 6;
	}


	state->flag.ac = (answer > 0x0F);

	state->A = answer;

	if (((state->A & 0xFF00) > 9) || (state->flag.C))
	{
		answer = state->A + 6;
	}

	state->flag.C = (answer > 0xFF);

	state->flag.S = ((answer & 0x80) != 0);
	state->flag.Z = ((answer & 0xFF) == 0);
	state->flag.P = bParity(answer & 0xFF);

	state->pc += 1;
}

#include <stdio.h>

#include "branch.h"

// if (condition)
//	*PC = *(byte 3) *(byte 2)
void mne_J(State* state, BYTE conditionCode, BYTE* opcode)
{
	BYTE pass;

	switch (conditionCode)
	{
		case 0: pass = 1;                                       break; // Unconditional ("JMP")
		case 1: (state->flag.Z == 0) ? (pass = 1) : (pass = 0); break; // Not zero ("JNZ")
		case 2: (state->flag.Z == 1) ? (pass = 1) : (pass = 0); break; // Zero ("JZ")
		case 3: (state->flag.C == 0) ? (pass = 1) : (pass = 0); break; // No carry ("JNC")
		case 4: (state->flag.C == 1) ? (pass = 1) : (pass = 0); break; // Carry ("JC")
		case 5: (state->flag.P == 0) ? (pass = 1) : (pass = 0); break; // Odd parity ("JPO")
		case 6: (state->flag.P == 1) ? (pass = 1) : (pass = 0); break; // Even parity ("JPE")
		case 7: (state->flag.S == 0) ? (pass = 1) : (pass = 0); break; // Positive ("JP")
		case 8: (state->flag.S == 1) ? (pass = 1) : (pass = 0); break; // Negative ("JM")
	}

	if (pass)
	{
		state->pc = ((opcode[2] << 8) | opcode[1]);
	}
	else
	{
		state->pc += 3;
	}
}

// if (condition)
//	*(*SP - #1) = *PCH
//	*(*SP - #2) = *PCL
//	*SP = *SP - #2
//	*PC = *(byte 3) *(byte 2)
void mne_C(State* state, BYTE conditionCode, BYTE* opcode)
{
	BYTE pass;

	switch (conditionCode)
	{
		case 0: pass = 1;                                       break; // Unconditional ("CALL")
		case 1: (state->flag.Z == 0) ? (pass = 1) : (pass = 0); break; // Not zero ("CNZ")
		case 2: (state->flag.Z == 1) ? (pass = 1) : (pass = 0); break; // Zero ("CZ")
		case 3: (state->flag.C == 0) ? (pass = 1) : (pass = 0); break; // No carry ("CNC")
		case 4: (state->flag.C == 1) ? (pass = 1) : (pass = 0); break; // Carry ("CC")
		case 5: (state->flag.P == 0) ? (pass = 1) : (pass = 0); break; // Odd parity ("CPO")
		case 6: (state->flag.P == 1) ? (pass = 1) : (pass = 0); break; // Even parity ("CPE")
		case 7: (state->flag.S == 0) ? (pass = 1) : (pass = 0); break; // Positive ("CP")
		case 8: (state->flag.S == 1) ? (pass = 1) : (pass = 0); break; // Negative ("CM")
	}

	if (pass)
	{
		WORD pcl = state->pc & 0x00FF;
		WORD pch = state->pc & 0xFF00;
		state->memory[state->sp - 1] = (pch >> 8);
		state->memory[state->sp - 2] = pcl;

		state->sp -= 2;
		state->pc = ((opcode[2] << 8) | opcode[1]);
	}
	else
	{
		state->pc += 3;
	}
}

// if (condition)
//	*PCL = **SP
//	*PCH = *(*SP + #1)
//	*SP = *SP + #2
void mne_R(State* state, BYTE conditionCode)
{
	BYTE pass;

	switch (conditionCode)
	{
		case 0: pass = 1;                                       break; // Unconditional ("RET")
		case 1: (state->flag.Z == 0) ? (pass = 1) : (pass = 0); break; // Not zero ("RNZ")
		case 2: (state->flag.Z == 1) ? (pass = 1) : (pass = 0); break; // Zero ("RZ")
		case 3: (state->flag.C == 0) ? (pass = 1) : (pass = 0); break; // No carry ("RNC")
		case 4: (state->flag.C == 1) ? (pass = 1) : (pass = 0); break; // Carry ("RC")
		case 5: (state->flag.P == 0) ? (pass = 1) : (pass = 0); break; // Odd parity ("RPO")
		case 6: (state->flag.P == 1) ? (pass = 1) : (pass = 0); break; // Even parity ("RPE")
		case 7: (state->flag.S == 0) ? (pass = 1) : (pass = 0); break; // Positive ("RP")
		case 8: (state->flag.S == 1) ? (pass = 1) : (pass = 0); break; // Negative ("M")
	}

	if (pass)
	{
		WORD pcl = (state->pc & 0x00FF);
		WORD pch = (state->pc & 0xFF00);

		pcl = state->memory[state->sp];
		pch = state->memory[state->sp + 1];

		state->pc = (pch << 8 | pcl);
		state->sp += 2;
	}

	state->pc += 3;
}

// *(*SP - #1) = *PCH
// *(*SP - #2) = *PCL
// *SP = *SP - #2
// *PC = #8 * n
void mne_RST(State* state, BYTE n)
{
	WORD pcl = state->pc & 0x00FF;
	WORD pch = state->pc & 0xFF00;
	state->memory[state->sp - 1] = pch >> 8;
	state->memory[state->sp - 2] = pcl;

	state->sp -= 2;

	state->pc = 8 * n;
}

// *PCH = *H
// *PCL = *L
void mne_PCHL(State* state)
{
	WORD pcl = (state->pc & 0x00FF);
	WORD pch = (state->pc & 0xFF00);

	pch = state->H;
	pcl = state->L;
	state->pc = (pch << 8) | pcl;

	state->pc += 1;
}

#include "../util.h"

void mne_J(State* state, BYTE conditionCode, BYTE* opcode);

void mne_C(State* state, BYTE conditionCode, BYTE* opcode);

void mne_R(State* state, BYTE conditionCode);

void mne_RST(State* state, BYTE n);

void mne_PCHL(State* state);

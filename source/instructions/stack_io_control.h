#include <stdio.h>
#include <stdlib.h>

#include "../util.h"

void mne_PUSH(State* state, char regPair);

void mne_POP(State* state, char regPair);

void mne_XTHL(State* state);
void mne_SPHL(State* state);

void mne_IN(State* state, BYTE* data);
void mne_OUT(State* state, BYTE* data);

void mne_EI(State* state);
void mne_DI(State* state);

void mne_HLT(State* state);

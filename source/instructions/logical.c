#include "logical.h"

// *A = *A AND *r
void mne_ANA(State* state, char reg)
{
	// No need for answer to be a WORD because the carry flag is always cleared
	BYTE answer;
	WORD combined;
	switch (reg)
	{
		case 'A': answer = state->A & state->A; break;
		case 'B': answer = state->A & state->B; break;
		case 'C': answer = state->A & state->C; break;
		case 'D': answer = state->A & state->D; break;
		case 'E': answer = state->A & state->E; break;
		case 'H': answer = state->A & state->H; break;
		case 'L': answer = state->A & state->L; break;
		// *A = *A AND *(*H *L)
		case 'M': combined = (state->H << 8) | state->L;
			  answer = state->A & state->memory[combined];
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.P  = bParity(answer);
	state->flag.C  = 0;

	state->A = answer;

	state->pc += 1;
}

// *A = *A AND *(byte 2)
void mne_ANI(State* state, BYTE* opcode)
{
	BYTE answer = state->A & opcode[1];

	state->flag.S = ((answer & 0x80) != 0);
	state->flag.Z = (answer == 0);
	state->flag.P = bParity(answer);
	state->flag.C = 0;

	state->A = answer;

	state->pc += 2;
}

// *A = *A XOR *r
void mne_XRA(State* state, char reg)
{
	BYTE answer;
	WORD combined;
	switch (reg)
	{
		case 'A': answer = state->A ^ state->A; break;
		case 'B': answer = state->A ^ state->B; break;
		case 'C': answer = state->A ^ state->C; break;
		case 'D': answer = state->A ^ state->D; break;
		case 'E': answer = state->A ^ state->E; break;
		case 'H': answer = state->A ^ state->H; break;
		case 'L': answer = state->A ^ state->L; break;
		// *A = *A XOR *(*H *L)
		case 'M': combined = (state->H << 8) | state->L;
			  answer = state->A ^ state->memory[combined];
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = 0;
	state->flag.P  = bParity(answer);
	state->flag.C  = 0;

	state->A = answer;

	state->pc += 1;
}

// *A = *A XOR *(byte 2)
void mne_XRI(State* state, BYTE* opcode)
{
	BYTE answer = state->A ^ opcode[1];

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = 0;
	state->flag.P  = bParity(answer);
	state->flag.C  = 0;

	state->A   = answer;

	state->pc += 2;
}

// *A = *A OR *r
void mne_ORA(State* state, char reg)
{
	BYTE answer;
	WORD combined;

	switch (reg)
	{
		case 'A': answer = state->A | state->A; break;
		case 'B': answer = state->A | state->B; break;
		case 'C': answer = state->A | state->C; break;
		case 'D': answer = state->A | state->D; break;
		case 'E': answer = state->A | state->E; break;
		case 'H': answer = state->A | state->H; break;
		case 'L': answer = state->A | state->L; break;
		// *A = *A OR *(*H *L)
		case 'M': combined = (state->H << 8) | state->L;
			  answer = state->A | state->memory[combined];
			  break;
	}

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = 0;
	state->flag.P  = bParity(answer);
	state->flag.C  = 0;

	state->A = answer;

	state->pc += 1;
}

// *A = *A | *(byte 2)
void mne_ORI(State* state, BYTE* opcode)
{
	BYTE answer = state->A | opcode[1];

	state->flag.S  = ((answer & 0x80) != 0);
	state->flag.Z  = (answer == 0);
	state->flag.ac = 0;
	state->flag.P  = bParity(answer);
	state->flag.C  = 0;

	state->A   = answer;

	state->pc += 2;
}

// *A -- *r
// The content of reg is compared with the accumulator. Neither are changed.
// The Z flag is set if *A == *r. The C flag is set if *A < *r.
void mne_CMP(State* state, char reg)
{
	WORD combined;

	switch (reg)
	{
		case 'A': state->flag.Z = 1;
			  state->flag.C = 0;
			  break;
		case 'B': (state->A == state->B) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->B) ? (state->flag.C = 1) : (state->flag.C = 0);
			  break;
		case 'C': (state->A == state->C) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->C) ? (state->flag.C = 1) : (state->flag.C = 0);
			  break;
		case 'D': (state->A == state->D) ? (state->flag.Z = 1) : (state->flag.Z = 0); (state->A < state->D) ? 
			  (state->flag.C = 1) : (state->flag.C = 1); break;
		case 'E': (state->A == state->E) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->E) ? (state->flag.C = 1) : (state->flag.C = 0);
			  break;
		case 'H': (state->A == state->H) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->H) ? (state->flag.C = 1) : (state->flag.C = 0);
			  break;
		case 'L': (state->A == state->L) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->L) ? (state->flag.C = 1) : (state->flag.C = 0);
		// *A -- *(*H *L)
		case 'M': combined = (state->H << 8) | state->L;
			  (state->A == state->memory[combined]) ? (state->flag.Z = 1) : (state->flag.Z = 0);
			  (state->A <  state->memory[combined]) ? (state->flag.C = 1) : (state->flag.C = 0);
			  break;
	}

	state->flag.S = ((state->A & 0x80) != 0);
	state->flag.P = bParity(state->A);

	state->pc += 1;
}

// *A -- *(byte 2)
void mne_CPI(State* state, BYTE* opcode)
{
	(state->A == opcode[1]) ? (state->flag.Z = 1) : (state->flag.Z = 0);
	(state->A <  opcode[1]) ? (state->flag.C = 1) : (state->flag.C = 0);

	state->flag.S = ((state->A & 0x80) != 0);
	state->flag.P = bParity(state->A);

	state->pc += 2;
}

// *A_n+1 = *A_n,
// *A_0 = *A_7,
// *(carry) = *A_7
void mne_RLC(State* state)
{
	BYTE overflow = (state->A & 0x80) >> 7;
	state->A = (state->A << 1) | overflow;
	state->flag.C = overflow;

	state->pc += 1;
}

// *A_n = *A_n-1,
// *A_7 = *A_0,
// *(carry) = *A_0
void mne_RRC(State* state)
{
	BYTE underflow = (state->A & 0x01);
	state->A = (state->A >> 1) | (underflow << 7);
	state->flag.C = underflow;

	state->pc += 1;
}

// *A_n+1 = *A_n,
// *(carry) = *A_7,
// *A_0 = *(carry)
void mne_RAL(State* state)
{
	BYTE overflow = (state->A & 0x80) >> 7;
	state->A = (state->A << 1) | state->flag.C;
	state->flag.C = overflow;

	state->pc += 1;
}

// *A_n = *A_n+1,
// *(carry) = *A_0,
// *A_7 = *(carry)
void mne_RAR(State* state)
{
	BYTE underflow = (state->A & 0x01);
	state->A = (state->A >> 1) | (state->flag.C << 7);
	state->flag.C = underflow;

	state->pc += 1;
}

// *A = ~(*A)
void mne_CMA(State* state)
{
	state->A = ~(state->A);

	state->pc += 1;
}

// *(carry) = ~(*(carry))
void mne_CMC(State* state)
{
	state->flag.C = ~(state->flag.C);

	state->pc += 1;
}

// *(carry) = #1
void mne_STC(State* state)
{
	state->flag.C = 1;

	state->pc += 1;
}

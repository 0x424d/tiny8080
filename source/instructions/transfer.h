#include "../util.h"

void mne_MOV(State* state, char r1, char r2);
void mne_MVI(State* state, char reg, BYTE* opcode);

void mne_LDA(State* state, BYTE* opcode);
void mne_STA(State* state, BYTE* opcode);

void mne_LHLD(State* state, BYTE* opcode);
void mne_SHLD(State* state, BYTE* opcode);

void mne_LXI(State* state, char regPair, BYTE* opcode);

void mne_LDAX(State* state, char regPair);
void mne_STAX(State* state, char regPair);

void mne_XCHG(State* state);

#include "stack_io_control.h"

// *(*SP - 1) = *rh
// *(*SP - 2) = *rl
// *SP = *SP - 2
// P refers to the PUSH PSW command
void mne_PUSH(State* state, char regPair)
{
	switch (regPair)
	{
		case 'B': state->memory[state->sp - 1]  = state->B; state->memory[state->sp - 2] = state->C; break;
		case 'D': state->memory[state->sp - 1]  = state->D; state->memory[state->sp - 2] = state->E; break;
		case 'H': state->memory[state->sp - 1]  = state->H; state->memory[state->sp - 2] = state->L; break;
		case 'C': BYTE pcl = state->pc & 0x00FF;                          // PUSH PC
			  BYTE pch = state->pc & 0xFF00;
			  state->memory[state->sp - 1]  = pch >> 8;
			  state->memory[state->sp - 2]  = pcl;
		case 'P': state->memory[state->sp - 1]  = state->A;               // PUSH PSW
			  state->memory[state->sp - 2] |= (state->flag.C  << 0);
			  state->memory[state->sp - 2] |= (0x01           << 1);
			  state->memory[state->sp - 2] |= (state->flag.P  << 2);
			  state->memory[state->sp - 2] |= (0x00           << 3);
			  state->memory[state->sp - 2] |= (state->flag.ac << 4);
			  state->memory[state->sp - 2] |= (0x00           << 5);
			  state->memory[state->sp - 2] |= (state->flag.Z  << 6);
			  state->memory[state->sp - 2] |= (state->flag.S  << 7);
			  break;
	}

	state->sp -= 2;
	state->pc += 1;
}

// *rl = **SP
// *rh = *(*SP + 1)
// *SP = *SP + 2
void mne_POP(State* state, char regPair)
{
	switch (regPair)
	{
		// TODO: debug the annoying warning I keep getting originating from this case statement
		case 'B': state->C = state->memory[state->sp]; state->B = state->memory[state->sp + 1]; break;
		case 'D': state->E = state->memory[state->sp]; state->D = state->memory[state->sp + 1]; break;
		case 'H': state->L = state->memory[state->sp]; state->H = state->memory[state->sp + 1]; break;
		case 'P': state->flag.C  = (state->memory[state->sp]); state->flag.C  &= (0x01 << 0);
			  state->flag.P  = (state->memory[state->sp]); state->flag.P  &= (0x01 << 2);
			  state->flag.ac = (state->memory[state->sp]); state->flag.ac &= (0x01 << 4);
			  state->flag.Z  = (state->memory[state->sp]); state->flag.Z  &= (0x01 << 6);
			  state->flag.S  = (state->memory[state->sp]); state->flag.S  &= (0x01 << 7);
			  state->A       = (state->memory[state->sp + 1]);
			  state->sp     -= 2;
			  break;
	}

	state->pc += 1;
}

// *L = **SP
// *H = *(*SP + 1)
void mne_XTHL(State* state)
{
	state->L = state->memory[state->sp];
	state->sp += 1;
	state->H = state->memory[state->sp];

	state->pc += 1;
}

// *SP = *H *L
void mne_SPHL(State* state)
{
	state->sp = (state->H << 8) | state->L;

	state->pc += 1;
}

// *A = *data
void mne_IN(State* state, BYTE* port)
{
	state->A = *port;

	state->pc += 2;
}

// *data = *A
void mne_OUT(State* state, BYTE* port)
{
	*port = state->A;

	state->pc += 2;
}

void mne_EI(State* state)
{
	state->interrupt_enable = 1;

	state->pc += 1;
}

void mne_DI(State* state)
{
	state->interrupt_enable = 0;

	state->pc += 1;
}

void mne_HLT(State* state)
{
	printf("Received HLT instruction.");
	printf("Exiting.");
	exit(2);
}

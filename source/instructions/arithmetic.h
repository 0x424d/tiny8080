#include "../util.h"

void mne_ADD(State* state, char reg);
void mne_ADI(State* state, BYTE* opcode);
void mne_ADC(State* state, char reg);
void mne_ACI(State* state, BYTE* opcode);

void mne_SUB(State* state, char reg);
void mne_SUI(State* state, BYTE* opcode);
void mne_SBB(State* state, char reg);
void mne_SBI(State* state, BYTE* opcode);

void mne_INR(State* state, char reg);
void mne_DCR(State* state, char reg);

void mne_INX(State* state, char regPair);
void mne_DCX(State* state, char regPair);
void mne_DAD(State* state, char regPair);

void mne_DAA(State* state);

#include "transfer.h"

// *r1 = *r2
void mne_MOV(State* state, char r1, char r2)
{
	// Same as a NOP. MOV M, M is handled specially, with the HLT mnemonic
	if (r1 == r2)
	{
		return;
	}

	BYTE setTo;
	WORD combined;

	switch (r2)
	{
		case 'A': setTo = state->A; break;
		case 'B': setTo = state->B; break;
		case 'C': setTo = state->C; break;
		case 'D': setTo = state->D; break;
		case 'E': setTo = state->E; break;
		case 'H': setTo = state->H; break;
		case 'L': setTo = state->L; break;
		// *r1 = *(*H *L)
		case 'M': combined = (state->H << 8) | (state->L);
			  switch (r1)
			  {
			  	case 'A': state->A = state->memory[combined]; break;
				case 'B': state->B = state->memory[combined]; break;
				case 'C': state->C = state->memory[combined]; break;
				case 'D': state->D = state->memory[combined]; break;
				case 'E': state->E = state->memory[combined]; break;
				case 'H': state->H = state->memory[combined]; break;
				case 'L': state->L = state->memory[combined]; break;
				// There is no MOV M, M statement
			  }
			  break;
	}

	switch (r1)
	{
		case 'A': state->A = setTo; break;
		case 'B': state->B = setTo; break;
		case 'C': state->C = setTo; break;
		case 'D': state->D = setTo; break;
		case 'E': state->E = setTo; break;
		case 'H': state->H = setTo; break;
		case 'L': state->L = setTo; break;
		// *(*H *L) = *r2
		case 'M': combined = (state->H << 8) | (state->L);
			  switch (r2)
			  {
			  	case 'A': state->memory[combined] = state->A; break;
				case 'B': state->memory[combined] = state->B; break;
				case 'C': state->memory[combined] = state->C; break;
				case 'D': state->memory[combined] = state->D; break;
				case 'E': state->memory[combined] = state->E; break;
				case 'H': state->memory[combined] = state->H; break;
				case 'L': state->memory[combined] = state->L; break;
			  }
			  break;
	}

	state->pc += 1;
}

// *r = *(byte 2)
void mne_MVI(State* state, char reg, BYTE* opcode)
{
	WORD combined;

	switch (reg)
	{
		case 'A': state->A = opcode[1]; break;
		case 'B': state->B = opcode[1]; break;
		case 'C': state->C = opcode[1]; break;
		case 'D': state->D = opcode[1]; break;
		case 'E': state->E = opcode[1]; break;
		case 'H': state->H = opcode[1]; break;
		case 'L': state->L = opcode[1]; break;
		// *(*H *L) = *(byte 2)
		case 'M': combined = (state->H << 8) | (state->L);
			  state->memory[combined] = opcode[1];
			  break;
	}

	state->pc += 2;
}

// *A = *(*(byte 3) *(byte 2))
void mne_LDA(State* state, BYTE* opcode)
{
	ADDR address = (opcode[2] << 8) | opcode[1];

	state->A   = state->memory[address];

	state->pc += 3;
}

// *A = **rp
void mne_LDAX(State* state, char regPair)
{
	ADDR address;
	switch (regPair)
	{
		case 'B': address = (state->B << 8) | state->C;
			  state->A = state->memory[address];
			  break;
		case 'D': address = (state->D << 8) | state->E;
			  state->A = state->memory[address];
			  break;
	}

	state->pc += 1;
}

// *(*(byte 3) *(byte 2)) = *A
void mne_STA(State* state, BYTE* opcode)
{
	ADDR address = (opcode[2] << 8) | opcode[1];

	state->memory[address] = state->A;

	state->pc += 3;
}

// **rp = *A
void mne_STAX(State* state, char regPair)
{
	ADDR address;
	switch (regPair)
	{
		case 'B': address = (state->B << 8) | state->C;
			  state->memory[address] = state->A;
			  break;
		case 'D': address = (state->D << 8) | state->E;
			  state->memory[address] = state->A;
			  break;
	}

	state->pc += 1;
}

// *L = *(*(byte 3) *(byte 2)),
// *H = *(*(byte 3) *(byte 2) + #1)
void mne_LHLD(State* state, BYTE* opcode)
{
	ADDR address = (opcode[2] << 8) | opcode[1];

	state->L   = state->memory[address];
	state->H   = state->memory[address+1];

	state->pc += 3;
}

// *(*(byte 3) *(byte 2)) = *L,
// *(*(byte 3) *(byte 2) + #1) = *H
void mne_SHLD(State* state, BYTE* opcode)
{
	ADDR address = (opcode[2] << 8) | opcode[1];

	state->memory[address]   = state->L;
	state->memory[address+1] = state->H;

	state->pc += 3;
}

// *rh = *(byte 3),
// *rl = *(byte 2)
void mne_LXI(State* state, char regPair, BYTE* opcode)
{
	switch (regPair)
	{
		case 'B': state->B  = opcode[2]; state->C = opcode[1]; break;
		case 'D': state->D  = opcode[2]; state->E = opcode[1]; break;
		case 'H': state->H  = opcode[2]; state->L = opcode[1]; break;
		case 'S': state->sp = (opcode[2] << 8) |    opcode[1]; break;
	}

	state->pc += 3;
}

// *H = *D,
// *D = *H,
// *L = *E,
// *E = *L
void mne_XCHG(State* state)
{
	BYTE tmp = state->H;
	state->H = state->D;
	state->D = tmp;

	tmp = state->L;
	state->L = state->E;
	state->E = tmp;

	state->pc += 1;
}

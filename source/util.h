#pragma once

#include <stdint.h>

typedef uint8_t  BYTE;
typedef uint16_t WORD;
typedef uint16_t ADDR;
typedef uint32_t DWORD;

// The colon after the variable means it will only use this many bits
// Structure of the Flags byte: |S|Z|-|AC|-|P|-|C|
typedef struct Flags
{
	BYTE S:1;   // Sign (set to seventh bit of the result)
	BYTE Z:1;   // Zero (set if the result is equal to 0)
	BYTE ac:1;  // Auxiliary carry (used with BCD)
	BYTE P:1;   // Parity (set if the number of set bits in the result
		    // is even)
	BYTE C:1;   // Carry (set if a carry was needed in addition, or
		    // a borrow was needed in subtraction)
	BYTE pad:3; // Padding for unused bytes
} Flags;

typedef struct State
{
	BYTE A;                // Register A
	BYTE B;                // Register B
	BYTE C;                // Register C
	BYTE D;                // Register D
	BYTE E;                // Register E
	BYTE H;                // Register H
	BYTE L;                // Register L
	BYTE interrupt_enable; // Are interrupts enabled?
	BYTE* memory;          // Array of bytes representing 8080's memory
	WORD sp;               // Stack pointer
	WORD pc;               // Program counter
	struct Flags flag;     // Flags struct
} State;

BYTE bParity(BYTE b);

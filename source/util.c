#include "util.h"

BYTE bParity(BYTE b)
{
	BYTE parity = 0;
	BYTE setBits = 0;

	while (b)
	{
		setBits += (b & 0x01);
		b = b >> 1;
	}

	parity = ((setBits % 2) == 0);
	return parity;
}
